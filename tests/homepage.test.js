// Default helpers
import * as client_functions from '../helpers/client-functions';
import * as element_assertions from '../helpers/element-assertions';

// Page objects
import homepage from '../pages/homepage';

// Page helpers
import * as homepage_helpers from '../helpers/homepage.helpers';

fixture `Home page`
    .page `https://katalon-demo-cura.herokuapp.com/`;

test('Greets with headers', async t => {
    element_assertions.text(homepage.h1, 'CURA Healthcare Service');
    element_assertions.text(homepage.h3, 'We Care About Your Health');
});

test('Shows burger menu', async t => {
    element_assertions.presentAndVisible(homepage.burgerMenuBtn);
});

test('Shows appointment button', async t => {    
    element_assertions.presentAndVisible(homepage.appointmentBtn);
});

test('Links to Login page', async t => {
    await homepage_helpers.goToLogin();
    await t.expect(client_functions.getWindowHref())
        .eql('https://katalon-demo-cura.herokuapp.com/profile.php#login');
});

test('Appointment btn leads to login page', async t => {
    await t.click(homepage.appointmentBtn);
    await t.expect(client_functions.getWindowHref())
        .eql('https://katalon-demo-cura.herokuapp.com/profile.php#login');
})