// Default helpers
import * as client_functions from '../helpers/client-functions';
import * as element_assertions from '../helpers/element-assertions';

// Page objects
import loginpage from '../pages/loginpage';

// Page helpers
import * as loginpage_helpers from '../helpers/loginpage.helpers';

fixture `Login page`
    .page `https://katalon-demo-cura.herokuapp.com/profile.php`;

test('Greets with headers', async t => {
    element_assertions.text(loginpage.h2, 'Login');
    element_assertions.text(loginpage.plead, 'Please login to make appointment.');
});

test('Shows username field', async t => {
    element_assertions.presentAndVisible(loginpage.usernameField);
});

test('Shows password field', async t => {    
    element_assertions.presentAndVisible(loginpage.passwordField);
});

test('Shows login button', async t => {    
    element_assertions.presentAndVisible(loginpage.loginBtn);
});

test('Displays error on bad username', async t => {
    loginpage_helpers.doLoginAs('Jane Burton', 'BadPw123');
    element_assertions.presentAndVisible(loginpage.error);
    element_assertions.textContains(loginpage.error, 'Login failed! Please ensure the username and password are valid.');
});

test('Displays error on bad password', async t => {
    loginpage_helpers.doLoginAs('John Doe', 'BadPw123');
    element_assertions.presentAndVisible(loginpage.error);
    element_assertions.textContains(loginpage.error, 'Login failed! Please ensure the username and password are valid.');
});

test('Leads to appointment page on valid login', async t => {
    await loginpage_helpers.doLoginAs('John Doe', 'ThisIsNotAPassword');
    await t.expect(client_functions.getWindowHref())
        .eql('https://katalon-demo-cura.herokuapp.com/#appointment');
});