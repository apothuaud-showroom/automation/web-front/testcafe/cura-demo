// Default helpers
import * as element_assertions from '../helpers/element-assertions';

// Page objects
import homepage from '../pages/homepage';
import appointmentpage from '../pages/appointmentpage';

// Page helpers

// Authentication rols
import regularAuth from '../roles/regular';

fixture `Appointment page`
    .beforeEach(async t => {
        await t
            .useRole(regularAuth)
            .navigateTo('https://katalon-demo-cura.herokuapp.com/#appointment');
    });

test('Is like home page', async t => {
    element_assertions.text(homepage.h1, 'CURA Healthcare Service');
    element_assertions.text(homepage.h3, 'We Care About Your Health');
    element_assertions.presentAndVisible(homepage.burgerMenuBtn);
    element_assertions.presentAndVisible(homepage.appointmentBtn);
});

test('Shows appointment section', async t => {
    element_assertions.presentAndVisible(appointmentpage.section);
})

test('Greets with form header', async t => {
    element_assertions.presentAndVisible(appointmentpage.h2);
    element_assertions.textContains(appointmentpage.h2, 'Make Appointment');
})

test('Shows facility select', async t => {
    element_assertions.presentAndVisible(appointmentpage.facilitySelect);
})

test('Shows readmission checkbox', async t => {
    element_assertions.presentAndVisible(appointmentpage.readmissionCheckbox);
})

test('Shows medicare radio', async t => {
    element_assertions.presentAndVisible(appointmentpage.medicareRadio);
})

test('Shows medicaid radio section', async t => {
    element_assertions.presentAndVisible(appointmentpage.medicaidRadio);
})

test('Shows none radio section', async t => {
    element_assertions.presentAndVisible(appointmentpage.noneRadio);
})

test('Shows visit date field', async t => {
    element_assertions.presentAndVisible(appointmentpage.visitDateField);
})

test('Shows visit date calendar btn', async t => {
    element_assertions.presentAndVisible(appointmentpage.visitDateCalendar);
})

test('Shows comment text area', async t => {
    element_assertions.presentAndVisible(appointmentpage.commentTextArea);
})

test('Shows book btn', async t => {
    element_assertions.presentAndVisible(appointmentpage.bookBtn);
})

test('Facility options are 3', async t => {
    const expected = 3;
    const actual = await t.eval(() => {
        let options = document.querySelectorAll('#appointment #combo_facility option');
        return options.length;
    });
    await t.expect(actual).eql(expected);
})

test('Shows facility options \'Tokyo CURA Healthcare Center\'', async t => {
    await t.click(appointmentpage.facilitySelect);
    element_assertions.presentAndVisible(appointmentpage.facilityOptionTokyo);
})

test('Shows facility options \'Tokyo CURA Healthcare Center\'', async t => {
    await t.click(appointmentpage.facilitySelect);
    element_assertions.presentAndVisible(appointmentpage.facilityOptionHongKong);
})

test('Shows facility options \'Tokyo CURA Healthcare Center\'', async t => {
    await t.click(appointmentpage.facilitySelect);
    element_assertions.presentAndVisible(appointmentpage.facilityOptionSeoul);
})

test('Visit date calendar pops up on click field', async t  => {
    await t.click(appointmentpage.visitDateField);
    element_assertions.presentAndVisible(appointmentpage.datePickerDays);
})

test('Visit date calendar pops up on click calendar btn', async t  => {
    await t.click(appointmentpage.visitDateCalendar);
    element_assertions.presentAndVisible(appointmentpage.datePickerDays);
})

test('Visit date is mandatory', async t => {
    await t.click(appointmentpage.bookBtn);
    element_assertions.presentAndVisible(appointmentpage.datePickerDays);
})

test('Leads to appointment confirmation on submit', async t => {
    // complete form and submit
    // https://katalon-demo-cura.herokuapp.com/appointment.php#summary
    await t.expect(client_functions.getWindowHref())
        .eql('https://katalon-demo-cura.herokuapp.com/appointment.php#summary');
})