import { Selector } from 'testcafe';

class Page {
    constructor () {
        this.h1 = Selector('h1');
        this.h3 = Selector('h3');
        this.burgerMenuBtn = Selector('#menu-toggle');
        this.loginLnk = Selector('#sidebar-wrapper > ul > li:nth-child(4) > a');
        this.appointmentBtn = Selector('#make-appointment-button');
    }
}

export default new Page();