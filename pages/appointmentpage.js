import { Selector } from 'testcafe';

class Page {
    constructor () {
        this.section = Selector('#appointment');
        this.h2 = Selector('#appointment h2');
        this.facilitySelect = Selector('#appointment #combo_facility');
        this.facilityOption = Selector('#appointment #combo_facility option');
        this.facilityOptionTokyo = Selector('#appointment #combo_facility option[value=\'Tokyo CURA Healthcare Center\']');
        this.facilityOptionHongKong = Selector('#appointment #combo_facility option[value=\'Hongkong CURA Healthcare Center\']');
        this.facilityOptionSeoul = Selector('#appointment #combo_facility option[value=\'Seoul CURA Healthcare Center\']');
        this.readmissionCheckbox = Selector('#appointment #chk_hospotal_readmission');
        this.medicareRadio = Selector('#appointment #radio_program_medicare');
        this.medicaidRadio = Selector('#appointment #radio_program_medicaid');
        this.noneRadio = Selector('#appointment #radio_program_none');
        this.visitDateField = Selector('#appointment #txt_visit_date');
        this.visitDateCalendar = Selector('#appointment span.glyphicon-calendar');
        this.datePickerDays = Selector('div.datepicker-days');
        this.commentTextArea = Selector('#appointment #txt_comment');
        this.bookBtn = Selector('#appointment #btn-book-appointment')
    }
}

export default new Page();