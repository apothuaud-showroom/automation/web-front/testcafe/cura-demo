import { Selector } from 'testcafe';

class Page {
    constructor () {
        this.h2 = Selector('h2');
        this.plead = Selector('p.lead');
        this.usernameField = Selector('#txt-username');
        this.passwordField = Selector('#txt-password');
        this.loginBtn = Selector('#btn-login');
        this.error = Selector('p.text-danger');
    }
}

export default new Page();