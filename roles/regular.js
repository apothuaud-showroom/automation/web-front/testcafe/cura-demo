import { Role } from 'testcafe';
import * as loginpage_helpers from '../helpers/loginpage.helpers';

export default Role('https://katalon-demo-cura.herokuapp.com/profile.php', async t => {
    await loginpage_helpers.doLoginAs('John Doe', 'ThisIsNotAPassword');
});