import { t } from 'testcafe';
import loginpage from '../pages/loginpage';

export async function doLoginAs(username, password) {
    await t.typeText(loginpage.usernameField, username);
    await t.typeText(loginpage.passwordField, password);
    await t.click(loginpage.loginBtn);
};