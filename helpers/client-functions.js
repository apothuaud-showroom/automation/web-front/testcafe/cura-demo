import { ClientFunction } from 'testcafe';

export const getWindowLocation = ClientFunction(() => window.location);
export const getWindowHref = ClientFunction(() => window.location.href);

export const getElementComputedStyle = ClientFunction((target) => window.getComputedStyle(target));
export const getElementVisibility = ClientFunction((target) => window.getComputedStyle(target).visibility);