import { t } from 'testcafe';

export async function presentAndVisible (element) {
    const target = await element();
    const visibility = target.getStyleProperty('visibility');
    await t.expect(visibility).eql('visible');
}

export async function text (element, expected) {
    const target = await element();
    const actual = target.innerText;
    await t.expect(actual).eql(expected);
}

export async function textContains (element, expected) {
    const target = await element();
    const actual = target.innerText;
    await t.expect(expected).contains(actual);
}