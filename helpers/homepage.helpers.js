import { t } from 'testcafe';
import homePage from '../pages/homepage';

export async function goToLogin() {
    await t.click(homePage.burgerMenuBtn);
    await t.click(homePage.loginLnk);
};